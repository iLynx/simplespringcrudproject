package com.lynx.sscp.controller;

import com.lynx.sscp.model.Book;
import com.lynx.sscp.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author lynx
 */

@Controller
public class BookController {

    private BookService bookService;

    @Autowired(required = true)
    @Qualifier(value = "bookService")
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "booksMainPage", method = RequestMethod.GET)
    public String bookList(Model model){
        model.addAttribute("book", new Book());
        model.addAttribute("booksList", this.bookService.getListBooks());

        return "booksMainPage";
    }

    @RequestMapping(value = "/booksMainPage/add", method = RequestMethod.POST)
    public String addBook(@ModelAttribute("book") Book book){
        if(book.getId() == 0){
            this.bookService.addBook(book);
        } else {
            this.bookService.updateBook(book);
        }

        return "redirect:/booksMainPage";
    }

    @RequestMapping("/remove/{id}")
    public String deleteBook(@PathVariable("id") int id){
        this.bookService.deleteBook(id);

        return "redirect:/booksMainPage";
    }

    @RequestMapping("edit/{id}")
    public String editBook(@PathVariable("id") int id, Model model){
        model.addAttribute("book", this.bookService.getBookById(id));
        model.addAttribute("booksList", this.bookService.getListBooks());

        return "booksMainPage";
    }

    @RequestMapping("aboutBook/{id}")
    public String dataAboutBook(@PathVariable("id") int id, Model model){
        model.addAttribute("book", this.bookService.getBookById(id));

        return "aboutBook";
    }
}

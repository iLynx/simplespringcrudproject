package com.lynx.sscp.persistence;

import com.lynx.sscp.model.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author lynx
 */

@Repository
public class BookDaoImpl implements BookDao {


    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addBook(Book book) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(book);
        System.err.println("\n Book was successfully added. " + book.toString());
    }

    @Override
    public void updateBook(Book book) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(book);
        System.err.println("\n Book was successfully updated. " + book.toString());
    }

    @Override
    public void deleteBook(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Book book = getBookById(id);

        if(book != null){
            session.delete(book);
        }
        System.err.println("\n Book was successfully removed.");
    }

    @Override
    public Book getBookById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Book book = (Book) session.load(Book.class, new Integer(id));
        System.err.println("Book was successfully loaded. " + book.toString());

        return book;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> getListBooks() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Book> bookList = session.createQuery("from Book").list();

        return bookList;
    }
}

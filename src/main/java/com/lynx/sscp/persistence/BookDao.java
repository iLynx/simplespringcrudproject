package com.lynx.sscp.persistence;

import com.lynx.sscp.model.Book;

import java.util.List;

/**
 * @author lynx
 */
public interface BookDao {

    public void addBook(Book book);
    public void updateBook(Book book);
    public void deleteBook(int id);
    public Book getBookById(int id);
    public List<Book> getListBooks();
}

package com.lynx.sscp.service;

import com.lynx.sscp.model.Book;
import com.lynx.sscp.persistence.BookDao;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author lynx
 */

@Service
public class BookServiceImpl implements BookService {

    private BookDao bookDao;

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    @Transactional
    public void addBook(Book book) {
        this.bookDao.addBook(book);
    }

    @Override
    @Transactional
    public void updateBook(Book book) {
        this.bookDao.updateBook(book);
    }

    @Override
    @Transactional
    public void deleteBook(int id) {
        this.bookDao.deleteBook(id);
    }

    @Override
    @Transactional
    public Book getBookById(int id) {

        return this.bookDao.getBookById(id);
    }

    @Override
    @Transactional
    public List<Book> getListBooks() {

        return this.bookDao.getListBooks();
    }
}

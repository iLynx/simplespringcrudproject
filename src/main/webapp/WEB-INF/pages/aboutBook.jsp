<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
    <title>Simple Spring CRUD Project / information about book </title>
    <link rel="stylesheet" href="styles.css" type="text/css"/>
</head>
<body>

<h2>Detail info about book</h2>

<table align="center" border=1 class="table">
    <tr>
        <th width="40">id</th>
        <th width="120">Title</th>
        <th width="120">Author</th>
        <th width="150">Description</th>
        <th width="40">Price</th>
    </tr>
    <tr>
        <td>${book.id}</td>
        <td>${book.bookTitle}</td>
        <td>${book.bookAuthor}</td>
        <td>${book.description}</td>
        <td>${book.price}</td>
    </tr>
</table>

</body>
</html>

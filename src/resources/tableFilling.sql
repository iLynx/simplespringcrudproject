INSERT INTO BookCollection.books_list
(book_title, book_author, book_description, book_price)
VALUES
('first title', 'first author', 'first description', 35),
('second title', 'second author', 'second description', 62),
('third title', 'third author', 'third description', 94),
('fourth title', 'fourth author', 'fourth description', 35);

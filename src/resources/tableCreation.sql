CREATE TABLE BookCollection.books_list
(
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  book_title VARCHAR(80) NOT NULL,
  book_author VARCHAR(45) NOT NULL,
  book_description VARCHAR(150) NOT NULL,
  book_price INT NOT NULL
);

package com.lynx.sscp.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
/**
 * @author lynx
 */
public class TestBookGeneral {

    @Test
    public void testEmptyConstructor(){
        Book book = new Book();

        assertEquals("id", 0, book.getId());
        assertEquals("bookTitle", null, book.getBookTitle());
        assertEquals("bookAuthor", null, book.getBookAuthor());
        assertEquals("bookDescription", null, book.getDescription());
        assertEquals("bookPrice", 0, book.getPrice());
    }


    @Test
    public void testToString(){
        Book book = new Book();
        book.setBookTitle("SomeTitle");
        book.setBookAuthor("Tom Hardy");
        book.setDescription("some description");
        book.setPrice(35);

        String expectedResult = "Book{" +
                "id=0" +
                ", bookTitle='SomeTitle" + '\'' +
                ", bookAuthor='Tom Hardy" + '\'' +
                ", description='some description" + '\'' +
                '}';
        assertEquals(expectedResult, book.toString());
    }

    @Test
    public void testToStringEmptySet(){
        Book book = new Book();

        String expectedResult = "Book{" +
                "id=0" +
                ", bookTitle='null" + '\'' +
                ", bookAuthor='null" + '\'' +
                ", description='null" + '\'' +
                '}';
        assertEquals(expectedResult, book.toString());
    }

}
